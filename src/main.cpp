#include <iostream>


int main() {
	float valtemp = -28.6;
	int temp = 0;
	unsigned long octetH = 0;
	unsigned char octetHH = 0;
	unsigned char octetB = 0;
	unsigned char octetBH = 0;
	unsigned char cara[12];
	char caraH[12];

	

	for (int i = 0; i < 12; i++)
	{
		cara[i]=0;
	}
	if (valtemp < 0)
	{
		valtemp = valtemp * -1;
		cara[0]=0xF;
	}
	
	temp = valtemp * 10;				//multipliqation de la valeur pour récupérer la valeur au dixième prés
	octetH = temp & 0xFFF;				//masque pour récupérer octet Haut
	octetH = octetH >> 8;				//Décallage de l'octet Haut pour le faire passer en Bas
	octetHH = octetH & 0xF0;			//masque pour récupérer octet Haut de l'octet Haut
	octetHH = octetHH >> 4 ;			//Décallage de l'octet Haut de l'octet Haut en décallage
	cara[1]=octetHH;					//insertion de l'octet haut de l'octet haut dans le vecteur
	octetHH = octetH & 0x0F;			//Récupération de l'octet bas de l'octet Haut 
	cara[2]=octetHH;					//insertion de l'octet bas de l'octet haut dans le vecteur
	octetB = temp & 0x00FF;				//masque pour récupérer octet Bas
	octetBH = octetB & 0xF0;			//masque pour récupérer octet Haut de l'octet Bas
	octetBH = octetBH >> 4 ;			//Décallage de l'octet Haut de l'octet Bas 
	cara[3]=octetBH;					//insertion de l'octet haut de l'octet Bas dans le vecteur
	octetBH = octetB & 0x0F;			//Récupération de l'octet bas de l'octet Bas 
	cara[4]=octetBH;					//insertion de l'octet Bas de l'octet Bas dans le vecteur
	
	for (int i = 0; i < 12; i++)		//passage des valeur du vecteur en leur version ascii
	{
		if (cara[i] <= 9)
		{
			cara[i] = cara[i]+0x30;
		}else
		{
			cara[i] = cara[i]+55;
		}	
	}

	for (int i = 0; i < 12; i++)		//passage des valeur du vecteur en leur version ascii
	{
		caraH[i] = cara[i];
		std::cout<<caraH[i];
	}
	
}